import org.apache.kafka.clients.producer.ProducerRecord;
import com.gitee.jastee.kafka.producer.KafkaProducerClient;

/**
 * @Author jast
 * @Date 2020/4/19 下午4:18
 * @Version 1.0
 */
public class Test {
    public static void main(String[] args) throws InterruptedException {

        KafkaProducerClient kafkaProducerClient = new KafkaProducerClient();
        kafkaProducerClient.getProducer().send(new ProducerRecord<String,String>(
                "test","ttt"
        ));
        Thread.sleep(10000);
    }
}
