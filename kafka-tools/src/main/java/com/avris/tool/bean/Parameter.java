package com.avris.tool.bean;

import lombok.Data;

/**
 * @author Jast
 * @description 参数类
 * @date 2022-09-19 15:30
 */
@Data
public class Parameter {
    private String brokerList;
    private String topic;
    private String group;
    private Long beginTime;
    private Long endTime;
    private String content;
    private String type;
    private Boolean latest;
}
