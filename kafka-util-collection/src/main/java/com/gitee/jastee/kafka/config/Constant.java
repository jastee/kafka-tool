package com.gitee.jastee.kafka.config;

/**
 * @author jast
 * @date 2020/4/21 16:02
 */
public class Constant {
    /**
     * public final static String KAFKA_BROKERS= "10.16.0.2:9092";
     * public static final String KAFKA_BROKERS= "172.16.24.147:9092";
     */
    public final static String KAFKA_BROKERS= "10.16.0.2:9092";

    private Constant(){
        throw new IllegalStateException("Utility class");
    }
}
