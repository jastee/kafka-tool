package com.gitee.jastee.kafka.admin;

import com.gitee.jastee.kafka.util.Print;
import org.apache.kafka.clients.admin.DeleteRecordsResult;
import org.apache.kafka.clients.admin.DeletedRecords;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.TopicPartition;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public class KafkaAdminClientByJastTest {

    private KafkaAdminClientByJast kafkaAdminClientByJast;

    @Before
    public void before() {
        String brokerList = "172.16.24.147:9092";
        kafkaAdminClientByJast = new KafkaAdminClientByJast(brokerList);
        kafkaAdminClientByJast.setDetailPrint(true);
        System.out.println("Kafka admin client init , brokerList : " + brokerList);
    }

    /**
     * @Description: 指定offset删除该offset之前的所有数据
     * （从磁盘删除，删除策略和文件存储相关，当磁盘存储文件中最大Offset数据被删除，该文件才会被删除）
     * @Date: 2022/5/16
     **/
    @Test
    public void deleteRecordsByOffsetTest() {
        String topic = "ms-cdp-profile-topic";
        Map<Integer, Long> partitionAndOffset = new HashMap<>();
        partitionAndOffset.put(0, 16324699L);
        DeleteRecordsResult deleteRecordsResult = kafkaAdminClientByJast.deleteRecordsByOffset(topic, partitionAndOffset);
        Map<TopicPartition, KafkaFuture<DeletedRecords>> lowWatermarks = deleteRecordsResult.lowWatermarks();
        try {
            for (Map.Entry<TopicPartition, KafkaFuture<DeletedRecords>> entry : lowWatermarks.entrySet()) {
                DeletedRecords deletedRecords = entry.getValue().get();
                Print.println("Delete records by offset ,topic:"+entry.getKey().topic() + ",partition:" + entry.getKey().partition()
                        + ",offset:" + deletedRecords.lowWatermark());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * @Description: 创建Topic
     * @Date: 2022/5/16
     **/
    @Test
    public void createTopic() throws ExecutionException, InterruptedException {
        String topicName = "jast-test-"+System.currentTimeMillis();
        int numPartitions = 5;
        short replicationFactor = 2;
        kafkaAdminClientByJast.createTopic(topicName,numPartitions,replicationFactor);
    }

    /**
     * @Description: 创建Topic，指定配置参数
     * @Date: 2022/5/16
     **/
    @Test
    public void createTopicConfigs() throws ExecutionException, InterruptedException {
        String topicName = "jast-test-"+System.currentTimeMillis();
        int numPartitions = 5;
        short replicationFactor = 2;
        HashMap<String, String> configs = new HashMap<>();
        //设置1分钟数据过期
        configs.put("retention.ms","60000");
        kafkaAdminClientByJast.createTopic(topicName,numPartitions,replicationFactor,configs);
    }

    /**
     * @Description: 创建Topic，指定topic数据清理时间
     * @Date: 2022/5/16
     **/
    @Test
    public void createTopicRetentionMs() throws ExecutionException, InterruptedException {
        String topicName = "jast-test-"+System.currentTimeMillis();
        int numPartitions = 5;
        short replicationFactor = 2;
        kafkaAdminClientByJast.createTopic(topicName,numPartitions,replicationFactor,60000L);
    }

    /**
     * @Description: 查看Topic列表
     * @Date: 2022/5/16
     **/
    @Test
    public void topicList() throws ExecutionException, InterruptedException {
        kafkaAdminClientByJast.topicList();
    }


    /**
     * @Description: 查看Topic列表，包含内部主题
     * @Date: 2022/5/16
     **/
    @Test
    public void topicListContainInternal() throws ExecutionException, InterruptedException {
        kafkaAdminClientByJast.topicListContainInternal();
    }

    /**
     * @Description: 查看全部Topic元信息
     * @Date: 2022/5/16
     **/
    @Test
    public void describeTopics() throws ExecutionException, InterruptedException {
        kafkaAdminClientByJast.describeTopics();
    }

    /**
     * @Description: 查看指定Topic元信息
     * @Date: 2022/5/16
     **/
    @Test
    public void describeTopic() throws ExecutionException, InterruptedException {
        String topicName = "jast-test-1652680617308";
        kafkaAdminClientByJast.describeTopic(topicName);
    }

    /**
     * @Description: 删除topic
     * @Date: 2022/5/16
     **/
    @Test
    public void deleteTopic() throws ExecutionException, InterruptedException {
        String topicName = "jast-test-1652680720012";
        kafkaAdminClientByJast.deleteTopic(topicName);

        List<String> topicList = new ArrayList<>();
        topicList.add("jast-test-1652680828652");
        topicList.add("jast-test-1652680617308");
        kafkaAdminClientByJast.deleteTopics(topicList);
    }

    /**
     * @Description: 输出Topic详细配置信息
     * @Date: 2022/5/16
     **/
    @Test
    public void describeConfigTopic() throws ExecutionException, InterruptedException {
        String topicName = "jast-test-1652686098660";
        kafkaAdminClientByJast.describeConfigTopic(topicName);
    }

    /**
     * @Description: 修改Topic partition 数量，只能增加，不能减少
     * @Date: 2022/5/16
     **/
    @Test
    public void updateTopicPartition() throws ExecutionException, InterruptedException {
        kafkaAdminClientByJast.describeTopic("test_clueResultMsg");
        kafkaAdminClientByJast.updateTopicPartition("test_clueResultMsg",4);
        kafkaAdminClientByJast.describeTopic("test_clueResultMsg");

    }
    @After
    public void after() {
        if (kafkaAdminClientByJast != null) {
            kafkaAdminClientByJast.close();
            System.out.println("Kafka admin client closed");
        }
    }
}