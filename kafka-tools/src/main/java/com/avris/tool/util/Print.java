package com.avris.tool.util;


public class Print {

    public static Boolean isPrint = true;

    public static void println(Object string){
        if(isPrint){
            System.out.println(string.toString());
        }
    }

    public static void println(String string){
        if(isPrint){
            System.out.println(string);
        }
    }
}
