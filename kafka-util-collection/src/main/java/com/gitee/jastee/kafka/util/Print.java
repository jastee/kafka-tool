package com.gitee.jastee.kafka.util;

/**
 * @Description: TODO
 * @Author: Jast
 * @Date: 2022/5/16
 **/
public class Print {

    public static Boolean isPrint = true;

    public static void println(Object string){
        if(isPrint){
            System.out.println(string.toString());
        }
    }

    public static void println(String string){
        if(isPrint){
            System.out.println(string);
        }
    }
}
