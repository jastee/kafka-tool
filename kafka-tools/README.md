# Kafka消费工具类使用说明

## 启动方法
```
./start-kafka-tools --endTime=1663050091000 --brokerList=10.16.0.2:9092 --topic=userChange --group=test20220607 --beginTime=1663050090000 --content=雷逸,山西省阳泉市王怨淆惭虞信息有限责任公司
```

- 参数说明:
  
### 指定时间消费数据

|参数名|说明|是否必填|样例|
|---|---|---|---|
|brokerList|Kafka地址|是|--brokerList=10.16.0.2:9092|
|topic|Kafka Topic|是|--topic=userChange|
|beginTime|消费开始时间|是|--beginTime=1663050090000|
|endTime|消费结束时间|否|--endTime=1663050091000 |
|content|过滤包含内容,匹配多个使用逗号分隔，","|否|--content=1663050091000 |
|type|使用功能类型取值<br/>1.ConsumerForTime 指定时间消费<br/>2.Consumer |否|--type=ConsumerForTime |

### 直接消费数据
|参数名|说明|是否必填|样例|
|---|---|---|---|
|brokerList|Kafka地址|是|--brokerList=10.16.0.2:9092|
|topic|Kafka Topic|是|--topic=userChange|
|latest|是否从最新的数据开始消费|否（默认为False,消费最早的数据）|--latest=true|
|type|使用功能类型取值<br/>1.ConsumerForTime 指定时间消费<br/>2.Consumer |否|--type=Consumer |



## 消费保存数据位置
jar包所在目录下 data-$timesatmp.txt 文件