package com.gitee.jastee.kafka.util;

import org.apache.log4j.PropertyConfigurator;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author jast
 * @date 2020/4/21 11:01
 */
public class LogInit {

    public static void InitLog4jConfig() {

        Properties props = null;

        FileInputStream fis = null;

        try {

            // 从配置文件dbinfo.properties中读取配置信息

            props = new Properties();

            fis = new FileInputStream("log4j.properties");
//            fis = new FileInputStream("D:\\software\\idea\\ieda_workspace\\ho\\ho-dubbo\\conf\\log4j.properties");
          //  logger.info("加载log4j配置文件");
            props.load(fis);

            PropertyConfigurator.configure(props);//装入log4j配置信息

        } catch (Exception e) {

            e.printStackTrace();

        } finally {

            if (fis != null)

                try {

                    fis.close();

                } catch (IOException e) {

                    e.printStackTrace();

                }

            fis = null;

        }

    }
}
